package edu.duke.fintech.assignment4headsfirstjsp.web;

import edu.duke.fintech.assignment4headsfirstjsp.model.BeerExpert;

import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

@WebServlet(name = "beerServlet", value = "/selectbeer")
public class BeerSelect extends HttpServlet {
    private String message;

    public void init() {
        message = "Beer Selection Advice";
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException {
        response.setContentType("text/html");

        String c = request.getParameter("color");
        BeerExpert expert = new BeerExpert();
        List<String> beers = expert.getBrands(c);

        PrintWriter out = response.getWriter();
        out.println("<html><body>");
        out.println("<h1>" + message + "</h1>");
        out.println("Beer color: "+ c);
        out.println("<br>");
        out.println("Suggested Beers:<br>");
        for(String beer: beers) {
            out.println(beer);
            out.println("<br>");
        }
        out.println("</body></html>");
    }

    public void destroy() {
    }
}